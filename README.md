### What is this repository for? ###

This is a repo for API test task for Beable

### How do I get set up? ###

```
git clone https://anastasissimo@bitbucket.org/anastasissimo/beable-test-task.git 
npm install
```

## Tools
The Tools included are:

- [ts-node](https://github.com/TypeStrong/ts-node)
- [typescript](https://github.com/Microsoft/TypeScript)
- [mocha](https://mochajs.org/)
- [chai](http://chaijs.com/)
- [sinon](http://sinonjs.org/)
- [sinon-chai](https://github.com/domenic/sinon-chai)

## How to run
Simply type `npm run test` and all tests will be run.

## Configuration
All Mocha Configuration is handled via the [mocha.opts](./mocha.opts) File, this includes: