import {expect} from 'chai';
import {BoredHelper} from '../../main'
import {Activity} from '../../entities/Activity';
import {Error} from '../../entities/Error'

const noActivitiesErrorJson: Error = { error: "No activities found with the specified parameters" };
const failedToQueryErrorJson: Error = { error: "Failed to query due to error in arguments" };


describe('Test BoredAPI ', () => {
  it(
    'Test Successful request ',
    async () => {
      const activity = await BoredHelper.getActivity();
      const activityBody = activity.body as Activity;
      expect(activityBody.activity).not.to.be.empty;
    }
  );

  it(
    'Get Error while getting activity by type',
    async () => {
      const type = Math.random().toString();
      const activity = await BoredHelper.getActivityWithType(type);
      expect(activity.status).to.equal(200);
      expect(activity.body).to.deep.equal(noActivitiesErrorJson);
    }
  );

  it(
    'Get Activity by type',
    async () => {
      const type = "recreational";
      const activity = await BoredHelper.getActivityWithType(type);
      const activityBody = (activity.body as unknown) as Activity;
      expect(activity.status).to.equal(200);
      expect(activityBody.activity).not.to.be.empty;
      expect(activityBody.type).to.equal(type);
    }
  );

  it(
    'Get Activity by key',
    async () => {
      const key = '6197243';
      const activity = await BoredHelper.getActivityWithKey(key);
      const activityBody = (activity.body as unknown) as Activity;
      expect(activity.status).to.equal(200);
      expect(activityBody.key).to.equal(key);
    }
  );

  it(
    'Get Error while getting activity by type',
    async () => {
      const key = Math.random().toString();
      const activity = await BoredHelper.getActivityWithKey(key);
      expect(activity.status).to.equal(200);
      expect(activity.body).to.deep.equal(noActivitiesErrorJson);
    }
  );

  it(
    'Get Error while getting activity by price range',
    async () => {
      const minPrice = Math.random().toString()+'ABC';
      const maxPrice = 'salsaoao9qkjad0';
      const activity = await BoredHelper.getActivityWithPriceRange(minPrice, maxPrice);
      expect(activity.status).to.equal(200);
      expect(activity.body).to.deep.equal(failedToQueryErrorJson);
    }
  );
  it(
    'Get activity by price range',
    async () => {
      const minPrice = 1;
      const maxPrice = 1 + Math.floor(Math.random()*50);
      const activity = await BoredHelper.getActivityWithPriceRange(minPrice.toString(), maxPrice.toString());
      const activityBody = activity.body as Activity;
      expect(activity.status).to.equal(200);
      if(activityBody.price !== undefined) {
        expect(activityBody.price).not.to.be.greaterThan(minPrice);
        expect(activityBody.price).not.to.be.lessThan(minPrice);
      }
    }
  );

});
