import 'reflect-metadata';

import {Container} from "typedi";
import {BoredAPIHelper} from "./helpers/boredAPIHelper";

export const BoredHelper = Container.get(BoredAPIHelper);