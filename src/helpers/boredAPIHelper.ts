import {Service} from "typedi";
import chaiHttp = require("chai-http");
import chai = require("chai");

chai.use(chaiHttp);

const BASE_URL='http://www.boredapi.com';
const apiUrl = '/api/activity';

@Service()
export class BoredAPIHelper {
  constructor(
  ) {}

  public async postMethod(path: string): Promise<Response> {

    const request = chai
      .request(BASE_URL)
      .get(path);

    const response = await request;

    return (response as unknown) as Response;
  }

  public async getActivity(): Promise<Response> {
    return await this.postMethod(apiUrl);
  }

  public async getActivityWithType(type: string): Promise<Response> {
    return await this.postMethod(`${apiUrl}?type=${type}`);
  }

  public async getActivityWithKey(key: string): Promise<Response> {
    return await this.postMethod(`${apiUrl}?key=${key}`);
  }

  public async getActivityWithPriceRange(minPrice: string, maxPrice: string): Promise<Response> {
    return await this.postMethod(`${apiUrl}?minprice=${minPrice}&maxprice=${maxPrice}`);
  }
}