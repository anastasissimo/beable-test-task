
export interface Activity {
  activity?: string,
  accessibility?: string | number,
  type?: string,
  participants?: number,
  price?: string | number,
  link?: string,
  key?: string | number
}
